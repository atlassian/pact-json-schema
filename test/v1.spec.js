'use strict';

const testSchemaAgainstExamples = require('./support/test-schema-against-examples');

describe('pact-json-schema v1', () => {
    testSchemaAgainstExamples('schemas/v1/schema.json', 'test/examples/v1');
});
