'use strict';

const bump = require('gulp-bump');
const conventionalChangelog = require('gulp-conventional-changelog');
const eslint = require('gulp-eslint');
const exec = require('./gulp/exec');
const fs = require('fs');
const git = require('gulp-git');
const gulp = require('gulp');
const jasmine = require('gulp-jasmine');
const minimist = require('minimist');
const runSequence = require('run-sequence');

// eslint-disable-next-line no-sync
const getVersion = () => JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;

const options = minimist(process.argv.slice(2), {strings: ['type']});

const getBumpType = () => {
    const validTypes = [
        'major',
        'minor',
        'patch',
        'prerelease'
    ];

    if (validTypes.indexOf(options.type) === -1) {
        throw new Error(`You must specify a release type as one of (${validTypes.join(', ')}), e.g. "--type minor"`);
    }

    return options.type;
};

gulp.task('lint:commits', () =>
    exec('./node_modules/.bin/conventional-changelog-lint --from=3af123a --preset angular'));

gulp.task('lint:javascript', () => {
    const jsFiles = [
        'gulpfile.js',
        'test/**/*.js'
    ];

    return gulp.src(jsFiles)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('publish:bump', () =>
    gulp.src(['./package.json'])
        .pipe(bump({type: getBumpType()}))
        .pipe(gulp.dest('./')));

gulp.task('publish:changelog', () =>
    gulp.src('CHANGELOG.md', {buffer: false})
        .pipe(conventionalChangelog({preset: 'angular'}))
        .pipe(gulp.dest('./')));

gulp.task('publish:commit', () =>
    gulp.src('.')
        .pipe(git.add())
        .pipe(git.commit(`chore: release ${getVersion()}`)));

gulp.task('publish:npm', () => exec('npm publish'));

gulp.task('publish:push', (callback) => {
    git.push('origin', 'master', {args: '--tags'}, callback);
});

gulp.task('publish:tag', (callback) => {
    const version = getVersion();

    git.tag(version, `Created Tag for version: ${version}`, callback);
});

gulp.task('test', () => gulp.src('test/**/*.spec.js').pipe(jasmine()));

// Public tasks

gulp.task('default', [
    'lint:commits',
    'lint:javascript',
    'test'
]);

gulp.task('publish', (callback) => {
    runSequence(
        'default',
        'publish:bump',
        'publish:changelog',
        'publish:commit',
        'publish:tag',
        'publish:push',
        'publish:npm',
        callback
    );
});

gulp.task('watch', () => {
    gulp.watch([
        'schemas/**/*',
        'test/**/*'
    ], ['test']);
});
